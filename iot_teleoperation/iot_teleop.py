#!/usr/bin python
####################################################### 
##             uddin@dis.uniroma1.it                 ##
##            last update : Jan 29th, 2017           ##
#######################################################
##  Description: This program transforms the inputs  ## 
##      recieved from cloud and the calls the        ##
##	         commandline_teleop.py program.      ##
#######################################################

MAX_SPEED = 0.4
DEFAULT_MOVE_TIME = 1


#######################################################
##      DO NOT CHANGE ANYTHING AFTER THIS LINE!      ##
#######################################################

import roslib
import rospy 
import sys
import os
import time
from ubidots import ApiClient
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

rospy.init_node('iot_teleop',disable_signals=True)


#######################################################
##      Ubidots Cloud Service stuffs, Datasource,    ##
##      Variavle, Token, URL, connection etc         ##
#######################################################


my_token = "YOUR_OWN_TOKEN"
url = "https://things.ubidots.com/api/v1.6/"
ds_id = ''  #place Data source ID
var_id = '' #Place your Veriable ID

api = ApiClient(token= my_token, base_url = url)


#connect to a datasource

#datasource = api.get_datasource(ds_id)
#print 'connecting to',datasource, 'from ubidots.com'

#connect using try catch block (variable connection) 
try:
    my_variable = api.get_variable(var_id)
    my_variable.save_value({'value': 7})   #Rest the value with a nvalid value for safety 
except UbidotsError400 as e:
    print "General Description: %s; and the detail: %s" % (e.message, e.detail)
except UbidotsForbiddenError as e:
    print "For some reason my account does not have permission to read this variable"
    print "General Description: %s; and the detail: %s" % (e.message, e.detail)



#set the cloud command according to the received value



#######################################################
##      	for advance use!                     ##
##    ROS stuffs moving speed and moving time	     ##
##	Default Speed is 0.2 and time 1 sec	     ##
#######################################################

if (len(sys.argv) > 2):
	speed = float(sys.argv[2])
else:
	speed = 0
if (speed <= 0):
	speed = 0.2
elif (speed > MAX_SPEED):
	speed = MAX_SPEED

if (len(sys.argv) > 3):
	moveTime = float(sys.argv[3])
else:
	moveTime = 0.0
if (moveTime <= 0):
	moveTime = DEFAULT_MOVE_TIME


#######################################################
##      	Receive and execution                ##
##    continously received command from cloud 	     ##
##		and execute the operation	     ##
#######################################################


try:
	while True:
		last_value = my_variable.get_values(1)
		#print only the value from json msg in integer format
		command = int(last_value[0]['value']) 
		if (command == 1):
			print "  >> Command recieved: move forward\n"
			os.system("python command_teleop.py forward " + str(speed) + " " + str(moveTime))
		elif (command == 2):
			print "  >> Command recieved: move backward\n"
			os.system("python command_teleop.py backward " + str(speed) + " " + str(moveTime))
		elif (command == 4):
			print "  >> Command recieved: move right\n"
			os.system("python command_teleop.py right " + str(speed) + " " + str(moveTime))
		elif (command == 3):
			print "  >> Command recieved: move left\n"
			os.system("python command_teleop.py left " + str(speed) + " " + str(moveTime))
		elif (command == 0):
			print "  >> STOP <<\n"
			os.system("python command_teleop.py stop " + str(speed) + " " + str(moveTime))
		else:
			print "  >> Waiting for valid command!\n"
		time.sleep(0.5)

except KeyboardInterrupt:
	pass

